package com.demo.springboot.model;

import java.util.Arrays;

public class SudokuRepository {
    private int sudokuTable[][];

    public SudokuRepository() {
        this.sudokuTable = new SudokuCSVReadComponent().readCSV();
    }

    public int[][] getSudokuTable() {
        return sudokuTable;
    }

    @Override
    public String toString() {
        String sudoku="";
        for(int i=0;i<9;i++){
            for(int j=0;j<9;j++){
                sudoku+=Integer.toString(sudokuTable[i][j])+',';
            }
            sudoku+='\n';
        }
        return "\nSudokuRepository{" +
                "sudokuTable=\n" + sudoku +
                '}';
    }
}
