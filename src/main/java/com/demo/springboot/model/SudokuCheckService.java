package com.demo.springboot.model;

import com.demo.springboot.dto.BadDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SudokuCheckService {
    private BadDto badDto;
    @Autowired
    private SudokuRepository sudokuRepository;

    public BadDto getBadDto() {
        return badDto;
    }

    public SudokuCheckService(){
    }

    public boolean check(){

        sudokuRepository=new SudokuRepository();
        int[][] sudokuTable=sudokuRepository.getSudokuTable();

        //
        String lineIds="";
        String kolumnIds="";
        String areaIds="";
        //
        // row checker
        for(int row = 0; row < 9; row++) {
            for (int col = 0; col < 8; col++) {
                for (int col2 = col + 1; col2 < 9; col2++) {
                    if (sudokuTable[row][col] == sudokuTable[row][col2]) {
                        lineIds += Integer.toString(row+1) + ',';
                    }
                }
            }
        }

        // column checker
        for(int col = 0; col < 9; col++) {
            for (int row = 0; row < 8; row++) {
                for (int row2 = row + 1; row2 < 9; row2++){
                    if (sudokuTable[row][col] == sudokuTable[row2][col]) {
                        kolumnIds += Integer.toString(col+1) + ',';
                    }
                }
            }
        }

        // grid checker
        for(int row = 0; row < 9; row += 3) {
            for (int col = 0; col < 9; col += 3){
                // row, col is start of the 3 by 3 grid
                for (int pos = 0; pos < 8; pos++) {
                    for (int pos2 = pos + 1; pos2 < 9; pos2++) {
                        if (sudokuTable[row + pos % 3][col + pos / 3] == sudokuTable[row + pos2 % 3][col + pos2 / 3]) {
                            areaIds += '(' + Integer.toString(row/2) + ',' + Integer.toString(col/2)+')' + ',';
                        }
                    }
                }
            }
        }
        lineIds = cutLastChar(lineIds);
        kolumnIds = cutLastChar(kolumnIds);
        areaIds = cutLastChar(areaIds);
        badDto=new BadDto(lineIds,kolumnIds,areaIds);
        if(isSudokuBad()){
            return false;
        }
        else {
            return true;
        }
    }

    private String cutLastChar(String toCut){
        if (toCut != null && toCut.length() > 0 && toCut.charAt(toCut.length() - 1) == ',') {
            toCut = toCut.substring(0, toCut.length() - 1);
        }
        return toCut;
    }

    public Boolean isSudokuBad() {
        if(!badDto.getLineIds().isEmpty() || !badDto.getKolumnIds().isEmpty() || !badDto.getAreaIds().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }


}
