package com.demo.springboot.rest;

import com.demo.springboot.dto.BadDto;
import com.demo.springboot.model.SudokuCSVReadComponent;
import com.demo.springboot.model.SudokuCheckService;
import com.demo.springboot.model.SudokuRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value="/api")
public class SudokuApiController {
    private static final Logger LOG = LoggerFactory.getLogger(SudokuApiController.class);

    //private final MovieListDto movies;
    //@Autowired
    private SudokuCheckService sudokuCheckService;

    public SudokuApiController() {

    }

    @PostMapping("/sudoku/verify")
    public ResponseEntity<BadDto> getMovies() {
        sudokuCheckService =new SudokuCheckService();
        if(sudokuCheckService.check()){
            SudokuRepository sudokuRepository=new SudokuRepository();
            //LOG.info(sudokuRepository.toString());
            return ResponseEntity.ok().body(null);    // = new ResponseEntity<>(movies, HttpStatus.OK);
        }
        else{
            return ResponseEntity.badRequest().body(sudokuCheckService.getBadDto());
        }

    }


}
